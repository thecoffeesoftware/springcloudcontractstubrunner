Test    

    1. open root folder in terminal
    2. type:
        - mvn clean install 
        - docker build -t contract-test:1.0.0 -f stub-runner-dockerfile.dockerfile .
        - docker run contract-test:1.0.0 -p 14100:14100 -n contract-test
To test contracts without docker
    
    1.download stubrunner and put it into the root folder.  link: https://repo1.maven.org/maven2/org/springframework/cloud/spring-cloud-contract-stub-runner-boot/3.0.1/spring-cloud-contract-stub-runner-boot-3.0.1.jar
    2.open root folder in terminal
    3.type:
        - mvn clean install
        - java -jar ./spring-cloud-contract-stub-runner-boot-3.0.1.jar --stubrunner.ids="com.thecoffee:cloudcontract:+:14100" --stubrunner.repositoryRoot=stubs:file:///{REPLACE_WITH_ROOT_FOLDER_URL}/target --stubrunner.stubs-mode=LOCAL
Hint
    
    Replace {REPLACE_WITH_ROOT_FOLDER_URL} placeholder with your project's root folder url
    If you execute the terminal command on linux, do not use " after --stubrunner.ids= and use only // instead of /// when specifying the stubs repository folder
