FROM springcloud/spring-cloud-contract-stub-runner:3.0.1
EXPOSE 14100
COPY target/stubs stubs
ENTRYPOINT ["java", "-jar", "stub-runner-boot.jar", "--stubrunner.ids=com.thecoffee:cloudcontract:+:14100", "--stubrunner.repositoryRoot=stubs:file://stubs", "--stubrunner.stubs-mode=LOCAL"]
